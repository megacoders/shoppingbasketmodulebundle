<?php

namespace Megacoders\ShoppingBasketModuleBundle\Store;


use Megacoders\ShoppingBundle\Model\ShoppingProduct;
use Symfony\Component\HttpFoundation\Session\Session;

class SessionShoppingBasketStore implements ShoppingBasketStore
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var string
     */
    private $sessionVar;

    /**
     * SessionShoppingBasketStore constructor.
     * @param Session $session
     * @param string $sessionVar
     */
    public function __construct(Session $session, $sessionVar)
    {
        $this->session = $session;
        $this->sessionVar = $sessionVar;
    }

    /**
     * {@inheritdoc}
     */
    public function getAll()
    {
        return $this->session->get($this->sessionVar, []);
    }

    /**
     * {@inheritdoc}
     */
    function getForEntity($entityClass)
    {
        $products = [];
        $allProducts = $this->getAll();

        foreach ($allProducts as $product) {
            if ($product->getEntityDescriptor()->getEntityClass() == $entityClass) {
                $products[] = $product;
            }
        }

        return $products;
    }

    /**
     * {@inheritdoc}
     */
    public function get($productIdentifier)
    {
        $products = $this->getAll();
        return isset($products[$productIdentifier]) ? $products[$productIdentifier] : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getCount()
    {
        return count($this->getAll());
    }

    /**
     * {@inheritdoc}
     */
    public function getProductIdentifier(ShoppingProduct $product)
    {
        return sha1($product->getEntityDescriptor()->__toString());
    }

    /**
     * {@inheritdoc}
     */
    public function add(ShoppingProduct $product)
    {
        $productIdentifier = $this->getProductIdentifier($product);
        $products = $this->getAll();

        if (isset($products[$productIdentifier])) {
            $product = $products[$productIdentifier];
            $product->setQuantity($product->getQuantity() + 1);

        } else {
            $products[$productIdentifier] = $product;
        }

        $this->set($products);
    }

    /**
     * {@inheritdoc}
     */
    public function update(ShoppingProduct $product)
    {
        $productIdentifier = $this->getProductIdentifier($product);
        $products = $this->getAll();

        if (isset($products[$productIdentifier])) {
            $products[$productIdentifier] = $product;
        }

        $this->set($products);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(ShoppingProduct $product)
    {
        $this->removeByIdentifier($this->getProductIdentifier($product));
    }

    /**
     * {@inheritdoc}
     */
    public function removeByIdentifier($productIdentifier)
    {
        $products = $this->getAll();

        if (isset($products[$productIdentifier])) {
            unset($products[$productIdentifier]);
        }

        $this->set($products);
    }

    /**
     * @param ShoppingProduct[] $products
     */
    private function set(array $products)
    {
        $this->session->set($this->sessionVar, $products);
    }

    function clear()
    {
        $this->set([]);
    }
}

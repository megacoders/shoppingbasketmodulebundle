<?php

namespace Megacoders\ShoppingBasketModuleBundle\Store;


use Megacoders\ShoppingBundle\Model\ShoppingProduct;

interface ShoppingBasketStore
{
    /**
     * @return ShoppingProduct[]
     */
    function getAll();

    /**
     * @param string $entityClass
     * @return ShoppingProduct[]
     */
    function getForEntity($entityClass);

    /**
     * @return int
     */
    function getCount();

    /**
     * @param string $productIdentifier
     * @return ShoppingProduct|null
     */
    function get($productIdentifier);

    /**
     * @param ShoppingProduct $product
     * @return string
     */
    function getProductIdentifier(ShoppingProduct $product);

    /**
     * @param ShoppingProduct $product
     */
    function add(ShoppingProduct $product);

    /**
     * @param ShoppingProduct $product
     */
    function update(ShoppingProduct $product);

    /**
     * @param ShoppingProduct $product
     */
    function remove(ShoppingProduct $product);

    /**
     * @param string $productIdentifier
     */
    function removeByIdentifier($productIdentifier);

    function clear();
}

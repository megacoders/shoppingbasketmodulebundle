<?php

namespace Megacoders\ShoppingBasketModuleBundle\Form\Type;

use Megacoders\ShoppingBundle\Entity\ShoppingAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShoppingAccountType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, ['label' => 'admin.entities.shopping_account.first_name'])
            ->add('lastName', TextType::class,  ['label' => 'admin.entities.shopping_account.last_name'])
            ->add('phone', TextType::class,  ['label' => 'admin.entities.shopping_account.phone'])
            ->add('email', EmailType::class, ['label' => 'admin.entities.shopping_account.email'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => ShoppingAccount::class]);
    }

}

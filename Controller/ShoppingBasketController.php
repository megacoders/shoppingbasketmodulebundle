<?php

namespace Megacoders\ShoppingBasketModuleBundle\Controller;


use Doctrine\ORM\EntityManager;
use Megacoders\PageBundle\Controller\Module\BaseModuleController;
use Megacoders\PageBundle\Entity\PageBlock;
use Megacoders\ShoppingBasketModuleBundle\Form\Type\ShoppingAccountType;
use Megacoders\ShoppingBasketModuleBundle\Store\ShoppingBasketStore;
use Megacoders\ShoppingBundle\Entity\ShoppingAccount;
use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Megacoders\ShoppingBundle\Entity\ShoppingOrderItem;
use Megacoders\ShoppingBundle\Event\OrderCreatedEvent;
use Megacoders\ShoppingBundle\Exception\PaymentException;
use Megacoders\ShoppingBundle\Manager\ShoppingManager;
use Megacoders\ShoppingBundle\Model\ShoppingProduct;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class ShoppingBasketController extends BaseModuleController
{
    /**
     * @var ShoppingBasketStore
     */
    private $store;

    /**
     * @var ShoppingManager
     */
    private $shoppingManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->store = $container->get('shopping_basket.store');
        $this->shoppingManager = $container->get('shopping.manager.shopping_manager');
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
    }

    /**
     * {@inheritdoc}
     */
    public function configureRoutes($baseName, $baseUrl, PageBlock $block)
    {
        $collection = new RouteCollection();

        /** List of products */
        $collection->add($baseName, new Route($baseUrl));

        /** Remove product */
        $collection->add(
            $baseName .'_remove_product',
            new Route($baseUrl .'/remove/{identifier}', ['_action' => 'removeProduct'])
        );

        /** Set product quantity */
        $collection->add(
            $baseName .'_set_product_quantity',
            new Route($baseUrl .'/quantity/{identifier}/{quantity}', ['_action' => 'setProductQuantity'])
        );

        /** Set product parameters */
        $collection->add(
            $baseName .'_set_product_parameters',
            new Route($baseUrl .'/parameters/{identifier}', ['_action' => 'setProductParameters'])
        );

        /** Check out */
        $collection->add(
            $baseName .'_checkout',
            new Route($baseUrl .'/checkout', ['_action' => 'checkout'])
        );

        /** Confirmation */
        $collection->add(
            $baseName .'_confirmation',
            new Route($baseUrl .'/confirmation', ['_action' => 'confirmation'])
        );

        /** Clear */
        $collection->add(
            $baseName .'_clear',
            new Route($baseUrl .'/clear', ['_action' => 'clear'])
        );

        /** Order paid */
        $collection->add(
            $baseName .'_done',
            new Route($baseUrl .'/done/{order_id}', ['_action' => 'done'])
        );

        /** Order pay failed */
        $collection->add(
            $baseName .'_fail',
            new Route($baseUrl .'/fail/{order_id}', ['_action' => 'fail'])
        );

        /** Order pay pending */
        $collection->add(
            $baseName .'_pending',
            new Route($baseUrl .'/pending/{order_id}', ['_action' => 'pending'])
        );

        return $collection;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $discountService = $this->getDiscountService();
        if ($discountService) {
            $discountService->handle($request, $this->store->getAll());
        }

        return $this->render('index', [
            'products' => $this->store->getAll(),
            'module' => $this,
            'unavailableProducts' => $this->getUnavailableProducts($this->store->getAll())
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function widgetAction(Request $request)
    {
        return $this->render('widget', [
            'count' => $this->store->getCount(),
            'module' => $this
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function removeProductAction(Request $request)
    {
        $identifier = $request->get('identifier');

        if ($identifier != null) {
            $this->store->removeByIdentifier($identifier);
        }

        return $this->redirect($this->generateMainBlockUrl());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function setProductQuantityAction(Request $request)
    {
        $identifier = $request->get('identifier');
        $quantity = $request->get('quantity');

        if ($identifier != null && $product = $this->store->get($identifier)) {
            if ($this->isProductQuantityEditable($product)) {
                if ($quantity > 0) {
                    $product->setQuantity($quantity);
                    $this->store->update($product);

                } else {
                    $this->store->remove($product);
                }
            }
        }

        return $this->redirect($this->generateMainBlockUrl());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function setProductParametersAction(Request $request)
    {
        $identifier = $request->get('identifier');
        $parameters = $request->get('parameters');

        if ($identifier != null && $product = $this->store->get($identifier)) {
            if ($this->shoppingManager->isProductParametersEditable($product)) {
                $product = $this->shoppingManager->updateProductParameters($product, $parameters);
                $this->store->update($product);
            }
        }

        return $this->redirect($this->generateMainBlockUrl());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function checkoutAction(Request $request)
    {
        if ($this->store->getCount() > 0) {

            if (count($this->getUnavailableProducts($this->store->getAll()))) {
                return $this->redirect($this->generateMainBlockUrl());
            }

            $form = $this->createAccountForm();
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $account = $this->resolveAccount($form->getData());
                $order = $this->createOrder($account);

                $this->setCurrentOrderId($order);
                return $this->redirect($this->generateConfirmationUrl());

            }

            return $this->render('checkout', [
                'form' => $form->createView(),
                'module' => $this
            ]);
        }

        return $this->redirect($this->generateMainBlockUrl());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function confirmationAction(Request $request)
    {
        $orderId = $this->getCurrentActiveOrderId();

        if (!$orderId) {
            return $this->redirect($this->generateCheckoutUrl());
        }

        $order = $this->shoppingManager->findOrder($orderId);

        if (!$order) {
            return $this->redirect($this->generateCheckoutUrl());
        }

        if ($order->getStatus() != ShoppingOrder::STATUS_CREATED) {
            return $this->redirect($this->generateCheckoutUrl());
        }

        $paymentService = $this->getPaymentService($order);

        $doneUrl = $this->generateMainBlockUrl('done',
            ['order_id' => $order->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL);

        $failUrl = $this->generateMainBlockUrl('fail',
            ['order_id' => $order->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL);

        $pendingUrl = $this->generateMainBlockUrl('pending',
            ['order_id' => $order->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL);

        $form = method_exists($paymentService, 'getRedirectForm') ?
            $paymentService->getRedirectForm($order, $doneUrl, $failUrl, $pendingUrl) :
            $this->getDummyForm()
        ;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (method_exists($paymentService, 'makePaymentRequest')) {
                try {
                    return $paymentService->makePaymentRequest($order, $doneUrl, $failUrl);

                } catch (PaymentException $e) {
                    /** @var Session $session */
                    $session = $this->get('session');
                    $session->getFlashBag()->add('error', $e->getMessage());

                    return $this->redirect($failUrl);
                }
            }

            return $this->redirect($doneUrl);
        }

        return $this->render('confirmation', [
            'order' => $order,
            'form'  => $form->createView(),
            'module' => $this
        ]);
    }

    /**
     * @return Response
     */
    public function clearAction()
    {
        $this->store->clear();

        $discountService = $this->getDiscountService();
        if ($discountService) {
            $discountService->clear();
        }

        return $this->redirect($this->generateMainBlockUrl());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function doneAction(Request $request)
    {
        $this->store->clear();

        $discountService = $this->getDiscountService();
        if ($discountService) {
            $discountService->clear();
        }

        if (!$this->getCurrentOrderId()) {
            return $this->redirect($this->generateMainBlockUrl());
        }

        $order = $this->shoppingManager->findOrder($request->get('order_id'));
        $paymentService = $this->getPaymentService($order);

        $paymentService->handleDoneRequest($request);

        $this->clearCurrentOrderId();

        return $this->render('done');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function failAction(Request $request)
    {
        $order = $this->shoppingManager->findOrder($request->get('order_id'));
        $paymentService = $this->getPaymentService($order);

        $paymentService->handleFailRequest($request);

        $this->clearCurrentOrderId();

        /** @var Session $session */
        $session = $this->get('session');

        return $this->render('fail', ['errors' => $session->getFlashBag()->get('error')]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function pendingAction(Request $request)
    {
        $this->store->clear();

        $discountService = $this->getDiscountService();
        if ($discountService) {
            $discountService->clear();
        }

        if (!$this->getCurrentOrderId()) {
            return $this->redirect($this->generateMainBlockUrl());
        }

        $order = $this->shoppingManager->findOrder($request->get('order_id'));
        $paymentService = $this->getPaymentService($order);

        $paymentService->handlePendingRequest($request);

        $this->clearCurrentOrderId();

        return $this->render('pending');
    }

    /**
     * @param ShoppingProduct $product
     * @return null|string
     */
    public function generateProductRemoveUrl(ShoppingProduct $product)
    {
        return $this->generateMainBlockUrl('remove_product', [
            'identifier' => $this->store->getProductIdentifier($product)
        ]);
    }

    /**
     * @param ShoppingProduct $product
     * @param int $quantity
     * @return null|string
     */
    public function generateProductQuantityUrl(ShoppingProduct $product, $quantity)
    {
        return $this->generateMainBlockUrl('set_product_quantity', [
            'identifier' => $this->store->getProductIdentifier($product),
            'quantity' => $quantity
        ]);
    }

    /**
     * @param ShoppingProduct $product
     * @param array $parameters
     * @return null|string
     */
    public function generateProductParametersUrl(ShoppingProduct $product, array $parameters)
    {
        return $this->generateMainBlockUrl('set_product_parameters', [
            'identifier' => $this->store->getProductIdentifier($product),
            'parameters' => $parameters
        ]);
    }

    /**
     * @return null|string
     */
    public function generateCheckoutUrl()
    {
        return $this->generateMainBlockUrl('checkout');
    }

    /**
     * @return null|string
     */
    public function generateConfirmationUrl()
    {
        return $this->generateMainBlockUrl('confirmation');
    }

    /**
     * @return null|string
     */
    public function generateClearUrl()
    {
        return $this->generateMainBlockUrl('clear');
    }

    /**
     * @param ShoppingProduct $product
     * @return array
     */
    public function getProductDescription(ShoppingProduct $product)
    {
        return $this->shoppingManager->getProductDescription($product);
    }

    /**
     * @return bool
     */
    public function isQuantityEditable() {
        $products = $this->store->getAll();

        foreach ($products as $product) {
            if ($this->isProductQuantityEditable($product)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param ShoppingProduct $product
     * @return bool
     */
    public function isProductQuantityEditable(ShoppingProduct $product)
    {
        return $this->shoppingManager->isProductQuantityEditable($product);
    }

    /**
     * @param ShoppingProduct $product
     * @return bool
     */
    public function isProductParametersEditable(ShoppingProduct $product)
    {
        return $this->shoppingManager->isProductParametersEditable($product);
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        $price = 0.0;
        $products = $this->store->getAll();

        foreach ($products as $product) {
            $price += $product->getTotalPrice();
        }

        return $price;
    }

    /**
     * @return Form
     */
    protected function createAccountForm()
    {
        $account = null;
        if ($orderId = $this->getCurrentActiveOrderId()) {
            $order = $this->shoppingManager->findOrder($orderId);
            if ($order) {
                $account = $order->getAccount();
            }
        }

        if ($account == null) {
            $account = new ShoppingAccount($this->getUser());
        }
        return $this->createForm(ShoppingAccountType::class, $account);
    }

    /**
     * @param ShoppingAccount $account
     * @return ShoppingAccount
     */
    protected function resolveAccount(ShoppingAccount $account)
    {
        $existingAccount = $this->shoppingManager->findAccount($account->getHash());

        if ($existingAccount !== null) {
            return $existingAccount;
        }

        return $account;
    }

    /**
     * @param ShoppingAccount $account
     * @return ShoppingOrder
     */
    protected function createOrder(ShoppingAccount $account)
    {
        $order = null;
        $discountService = $this->getDiscountService();

        if ($orderId = $this->getCurrentActiveOrderId()) {
            $order = $this->shoppingManager->findOrder($orderId);
            if ($order->getStatus() != ShoppingOrder::STATUS_CREATED) {
                $order = null;
            }
        }

        if ($order == null) {
            $order = new ShoppingOrder();
        }

        $order->removeAllItems();
        $order->setDate(new \DateTime());

        $order
            ->setAccount($account)
            ->setUser($this->getUser())
            ;

        $products = $this->store->getAll();
        $orderPrice = 0;

        foreach ($products as $product) {
            $order->addItem(new ShoppingOrderItem($product));
            $orderPrice += $product->getTotalPrice();
        }

        $order->setPrice($orderPrice);
        if ($discountService) {
            $discountService->setShoppingOrderDiscount($order);
        }

        $order->setLocale($this->getLocale());

        $this->entityManager->persist($order);
        $this->entityManager->flush($order);

        $event = new OrderCreatedEvent($order->getId());
        $this->container->get('event_dispatcher')
            ->dispatch(OrderCreatedEvent::NAME, $event);

        return $order;
    }

    /**
     * @param ShoppingProduct[] $products
     * @return ShoppingProduct[]
     */
    protected function getUnavailableProducts($products)
    {
        $shoppingManager = $this->shoppingManager;
        $orderId         = $this->getCurrentActiveOrderId();

        return array_filter($products, function ($product) use ($shoppingManager, $orderId) {
            return !$shoppingManager->isProductAvailable($product, $orderId);
        });
    }

    /**
     * @param ShoppingOrder $order
     */
    protected function setCurrentOrderId(ShoppingOrder $order)
    {
        /** @var Session $session */
        $session = $this->get('session');
        $session->set('order_id', $order->getId());
    }


    /**
     * @return int | null
     */
    protected function getCurrentActiveOrderId()
    {
        /** @var Session $session */
        $session = $this->get('session');
        $orderId =  $session->get('order_id');

        if ($orderId) {
            $order = $this->shoppingManager->findOrder($orderId);
            if ($order->getStatus() != ShoppingOrder::STATUS_CREATED) {
                $this->clearCurrentOrderId();
                return null;
            }
        }

        return $orderId;
    }

    /**
     * @return int
     */
    protected function getCurrentOrderId()
    {
        /** @var Session $session */
        $session = $this->get('session');
        return $session->get('order_id');
    }


/**
     *
     */
    protected function clearCurrentOrderId()
    {
        /** @var Session $session */
        $session = $this->get('session');
        $session->remove('order_id');
    }

    /**
     * @return Form
     */
    protected function getDummyForm()
    {
        return $this->container->get('form.factory')
            ->createNamedBuilder('', 'form')->getForm();
    }

    /**
     * @param ShoppingOrder $shoppingOrder
     * @return \Megacoders\ShoppingBundle\Service\ShoppingPaymentService
     */
    protected function getPaymentService(ShoppingOrder $shoppingOrder)
    {
        return ($shoppingOrder->getPrice() > 0 ?
            $this->shoppingManager->getPaymentService() :
            $this->shoppingManager->getStubPaymentService()
        );
    }

    /**
     * @return object
     */
    protected function getDiscountService()
    {
        return $this->shoppingManager->getDiscountService();
    }
}
